from pyliquibase import Pyliquibase

liquibase = Pyliquibase(defaultsFile="resources/liquibase.properties", logLevel="INFO")
# call execute with arguments
liquibase.execute("status")
# or
liquibase.validate()
liquibase.status()
liquibase.updateSQL()
liquibase.update()
# liquibase maintenance commands
liquibase.changelog_sync()
liquibase.clear_checksums()
liquibase.release_locks()