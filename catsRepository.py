import psycopg2
import liquibase
from dataBaseUtils import zwrocListePierwszychElementow

conn = psycopg2.connect(
    dbname='catsweb',
    user='catswebuser',
    host='flask_db',
    password='catsystem'
)

def zapiszImie(imieWlasciciela):
    cursor = conn.cursor()
    insert_query = """INSERT INTO wlasciciele (id, imie) VALUES (nextval('id_wlasciciela_seq'), '%s');""" % (imieWlasciciela)
    cursor.execute(insert_query)
    conn.commit()

def sprawdzCzyImieWlascicielaIstniejeWBazie(imieWlasciciela):
    cursor = conn.cursor()
    insert_query = """SELECT EXISTS(SELECT * from wlasciciele WHERE imie = '%s');""" % (imieWlasciciela)
    cursor.execute(insert_query)
    wlasciciele = cursor.fetchall()
    return (wlasciciele[0][0])

def wyciagnijZdania():
    cursor = conn.cursor()
    select_query = """SELECT * from zdania_przedstawiajace_kotki ;"""
    cursor.execute(select_query)
    zdania = cursor.fetchall()
    return zwrocListePierwszychElementow(zdania)

def zapiszKotki(listaKotkow, imieWprowadzajacego):
    """ Saves provided cats from listaKotkow, associating them with an owner identified with imieWprowadzajacego """
    cursor = conn.cursor()
    insert_query = """INSERT INTO kotki  (id, imie, wlasciciele_id) values ((nextval('id_kotki_seq')), '%s', 
    (select id  from wlasciciele
    where imie = '%s'));""" % (listaKotkow, imieWprowadzajacego)
    cursor.execute(insert_query)
    conn.commit()



def zwrocIstniejaceKotki(imieWlasciciela):
    cursor = conn.cursor()
    select_query = """select kotki.imie from  kotki  join wlasciciele on 
    kotki.wlasciciele_id  = wlasciciele.id where wlasciciele.imie = '%s';""" % (imieWlasciciela)
    cursor.execute(select_query)
    kotkiKrotki = cursor.fetchall()
    kotkiLista = []
    for kotekKrotka in kotkiKrotki:
        kotkiLista.append(kotekKrotka[0])
    return kotkiLista

def zapisywanieKotkadoBazy(imieWlasciciela,imieKotka):
    cursor = conn.cursor()
    insert_query = """insert into kotki (id, imie, wlasciciele_id) values ((nextval('id_kotki_seq')), 
    '%s', (select id  from wlasciciele where imie = '%s'));""" % (imieKotka, imieWlasciciela)
    cursor.execute(insert_query)
    conn.commit()


def wprowadzanieImieniaIHaslaPoRazPierwszy(imieWlasciciela, haslo):
    cursor = conn.cursor()
    insert_query = """INSERT INTO wlasciciele (id, imie, haslo) 
    VALUES (nextval('id_wlasciciela_seq'), '%s', '%s') ;""" % (imieWlasciciela, haslo)
    cursor.execute(insert_query)
    conn.commit()

def sprawdzanieHaslaIstniejacegoUzytkownika(imieWlasciciela, haslo):
    cursor = conn.cursor()
    select_query = """select haslo from wlasciciele where imie = '%s' and haslo = '%s';"""% (imieWlasciciela, haslo)
    cursor.execute(select_query)
    daneLogowania = cursor.fetchall()
    if daneLogowania == []:
        return False
    else:
        return True

def usunKonto(imieWlasciciela):
    cursor = conn.cursor()
    delete_query = """delete from wlasciciele where imie = '%s';""" % (imieWlasciciela)
    cursor.execute(delete_query)
    conn.commit()













