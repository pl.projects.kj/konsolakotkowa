import pytest
from dataBaseUtils import zwrocListePierwszychElementow

class Test:

    def test_pusta_lista(self):
        assert zwrocListePierwszychElementow([]) == []

    def test_lista_jeden_element(self):
        assert zwrocListePierwszychElementow([('bla',)]) == ['bla']

    def test_lista_dwa_element(self):
        assert zwrocListePierwszychElementow([('bla',),('lala',)]) == ['bla', 'lala']
