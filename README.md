Ten projekt implementuje aplikację do przechowywania własności kotków.
Aplikacja działa w dwóch trybach. 
Pierwszy tryb tworzy obraz Dockera (należy mieć go zainstalowanego), 
który uruchamia bazę PostgreSQL’ową oraz Flaska z docelową aplikacją. 
Używając np. POSTMAN’a możemy odpytywać endpointy w ramach protokołu HTTP.
Aplikację w tym trybie uruchamiamy następująco:

1. Wpisujemy polecenie   
docker compose build   
w ścieżce projektu, aby zbudować obraz aplikacji.  
2. Wpisujemy polecenie   
docker compose -f docker-compose.yml up -d   
w ścieżce projektu  
3. Uruchamiamy klienta SQL i narzędzie do administrowania bazą danych np. DBeaver i logujemy się
następującymi danymi:
host: localhost,
port: 5433, 
database: catsweb, 
username: catswebuser, 
password: catsystem 
Wszystkie tabele powinny być już wykonane dzięki Liquibase
4. W folderze Postman znajdują się pliki z danymi środowiskowymi, które należy zaimportować do programu Postman.
5. Endpointy należy odpytywać w takiej kolejności w jakiej są ponumerowane.
6. Wpisujemy polecenie
docker compose -f docker-compose.yml down
aby wyłączyć dockera i aplikację.

Tryb konsolowy
W kodzie można spotkać się z fragmentami, które nie biorą udziału w obsłudze żądań.
Są to pozostałości wersji trybu konsolowego aplikacji, który to już nie jest rozwijany (deprecated).

