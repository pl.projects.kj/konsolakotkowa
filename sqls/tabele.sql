CREATE TABLE wlasciciele (
    ID BIGINT PRIMARY KEY,
    imie TEXT NOT NULL,
    haslo char(32));
CREATE SEQUENCE id_wlasciciela_seq START 1;


create table kotki (
imie text not null, id bigint,
PRIMARY KEY (id),wlasciciele_id bigint,
FOREIGN KEY (wlasciciele_id)
REFERENCES wlasciciele(id)
ON DELETE CASCADE);
CREATE SEQUENCE id_kotki_seq START 1;


CREATE TABLE zdania_przedstawiajace_kotki (
zdanie text not null);
insert into zdania_przedstawiajace_kotki (zdanie) values ('Oto kotek %s jest bardzo grzeczny'),
('Powitaj kotka %s to wspaniały pieszczuch'),
('Hej kotku %s milusiński przyjacielu'),
('Cześć nowy przyjacielu o imieniu %s będzie nam bardzo miło!'),
('Dzień dobry %s baw się dobrze!');

