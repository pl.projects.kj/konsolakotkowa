import pytest
from ustaleniePowitania import ustalaniePowitania


class Test:

    def test_wyjatek(self):
        wyjatek = "Sasza"
        result = ustalaniePowitania(wyjatek)
        assert result == "Hej, dozorco!"
    def test_meskie_imie(self):
        meskieImie = "Damian"
        result = ustalaniePowitania(meskieImie)
        assert result == "Hej, dozorco!"

    def test_zenskie_imie(self):
        zenskieImie = "Ala"
        result = ustalaniePowitania(zenskieImie)
        assert result == "Hej, dozorczynio!"

    def test_binarne_imie(self):
        binarneImie = ["Olo", "Mare", "Lolu", "Dary"]
        for imie in binarneImie:
            result = ustalaniePowitania(imie)
            assert result == "Hej, dozorcze!"

    def test_zenskie_imie_z_I_na_koncu(self):
        zenskie_Imie_z_I_na_koncu = ["Maroi", "Karei", "Molui", "Polyi", "Tarai", "Zalii"]
        for imie in zenskie_Imie_z_I_na_koncu:
            result = ustalaniePowitania(imie)
            assert result == "Hej, dozorczynio!"
    def test_meski_imie_z_I_na_koncu(self):
        meskie_Imie_z_I_na_koncu = "Kali"
        result = ustalaniePowitania(meskie_Imie_z_I_na_koncu)
        assert result == "Hej, dozorco!"
