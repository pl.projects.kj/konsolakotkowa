--liquibase formatted sql
--changeset {Catsja}:{insertowanieZdan}

insert into zdania_przedstawiajace_kotki (zdanie) values
('Oto kotek %s jest bardzo grzeczny'),
('Powitaj kotka %s to wspaniały pieszczuch'),
('Hej kotku %s milusiński przyjacielu'),
('Cześć nowy przyjacielu o imieniu %s będzie nam bardzo miło!'),
('Dzień dobry %s baw się dobrze!');
