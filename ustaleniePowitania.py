
def ustalaniePowitania(imieWprowadzajacego):
    if imieWprowadzajacego == 'Sasza':
        return "Hej, dozorco!"
    elif imieWprowadzajacego[-1] == 'a':
        return "Hej, dozorczynio!"
    elif imieWprowadzajacego[-1] == 'o' or imieWprowadzajacego[-1] == 'e' or \
            imieWprowadzajacego[-1] == 'u' or imieWprowadzajacego[-1] == 'y':
        return "Hej, dozorcze!"
    elif imieWprowadzajacego[-1] == 'i':
        if imieWprowadzajacego[-2] == 'o' or imieWprowadzajacego[-2] == 'e' or \
                imieWprowadzajacego[-2] == 'u' or imieWprowadzajacego[-2] == 'y' or imieWprowadzajacego[-2] == 'a' \
                or imieWprowadzajacego[-2] == 'i':
            return "Hej, dozorczynio!"
        else:
            return "Hej, dozorco!"
    else:
        return "Hej, dozorco!"