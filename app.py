from flask import Flask, Blueprint, jsonify, request, json
import catConsole
import logowanie
import logging

app = Flask(__name__)

@app.route('/registration', methods=['POST'])
def registration():
    requestBody = request.json
    imieWprowadzajacego = (requestBody['login'])
    haslo = (requestBody['haslo'])
    logowanie.rejestracjaDlaHTTP(imieWprowadzajacego, haslo)
    return 'OK', 200

@app.route('/dodawanieKotka', methods=['POST'])
def dodawanieKotka():
    requestHeaders = request.headers

    loginIHaslo = (requestHeaders['Authorization'][6:]).split(':')
    login = loginIHaslo[0]
    haslo = loginIHaslo[1]


    if catConsole.sprawdzanieCzyUzytkownikIstniejeDlaHTTP(login) == True:

        if logowanie.czyDaneLogowaniaSaPoprawneDlaHTTP(login, haslo) == False:
            return 'niepoprawne dane logowania', 401
        requestBody = request.json
        dodanyKotek = (requestBody['imieKotka'])
        catConsole.dodawanieKotkadlaHTTP(dodanyKotek, login)
        return 'OK', 200
    else:
        print("Kotek dla użytkownika nie może zostać dodany, ponieważ dany użytkownk nie istnieje")
        return 'załóż konto', 401

@app.route('/pozyskiwanieKotkowzListy', methods=['GET'])
def pozyskiwanieKotkowzListy():
    requestHeaders = request.headers

    loginIHaslo = (requestHeaders['Authorization'][6:]).split(':')
    login = loginIHaslo[0]
    haslo = loginIHaslo[1]
    if catConsole.sprawdzanieCzyUzytkownikIstniejeDlaHTTP(login) == True:
        if logowanie.czyDaneLogowaniaSaPoprawneDlaHTTP(login, haslo) == False:
            return 'niepoprawne dane logowania', 401

    kotki = (catConsole.listaKotkowDlaHTTP(login))
    kotki_body = {"listaKotkow" : kotki}
    return kotki_body, 200

@app.route('/usuwanieUzytkownikazJegoKotkami', methods=['DELETE'])
def usuwanieUzytkownikazJegoKotkami():
    requestHeaders = request.headers
    logging.debug(requestHeaders)
    loginIHaslo = (requestHeaders['Authorization'][6:]).split(':')
    login = loginIHaslo[0]
    haslo = loginIHaslo[1]
    if catConsole.sprawdzanieCzyUzytkownikIstniejeDlaHTTP(login) == True:
        if logowanie.czyDaneLogowaniaSaPoprawneDlaHTTP(login, haslo) == False:
            return 'niepoprawne dane logowania', 401
        catConsole.usunKonto(login)
        return 'konto zostało usunięte', 200
    else:
        return 'konto nie istnieje', 401