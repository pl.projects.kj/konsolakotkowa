FROM python:3.9-bullseye

WORKDIR /app

RUN mkdir -p /usr/share/man/man1 /usr/share/man/man2 && \
    apt-get update &&\
    apt-get install -y --no-install-recommends openjdk-11-jre

COPY requirements.txt ./

RUN pip install -r requirements.txt

ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/

COPY . .

EXPOSE 4000

CMD [ "flask", "run", "--host=0.0.0.0", "--port=4000"]